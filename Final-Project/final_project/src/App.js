import './style/App.css';
import 'antd/dist/antd.css'
import Routes from './routes/Routes';
import {AppProvider} from './context/AppContext'

function App() {
  return (
    <>
      <AppProvider>
        <Routes/>
      </AppProvider>
    </>
  );
}

export default App;
