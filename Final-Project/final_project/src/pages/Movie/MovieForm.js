import {Input} from 'antd';
import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import { MovieContext } from '../../context/MovieContext';

const { TextArea } = Input;

const MovieForm = () => {

    const {states,Moviefunctions} = useContext(MovieContext)
    let {fetchById,functionSubmit,functionUpdate} = Moviefunctions
    let {input,setInput,
        currentIndex,setCurrentIndex
    }= states
    let {value} = useParams()

    useEffect(()=>{

        if(value!==undefined){
            fetchById(value)
        }

    },[])

    const handleSubmit = (event) => {
        event.preventDefault()
        if(currentIndex === -1){
            functionSubmit(currentIndex)
        }else{
            functionUpdate(currentIndex)
        }
        setInput({
            description:"",
            duration:0,
            genre:"",
            image_url:"",
            rating:0,
            review:"",
            title:"",
            year:1980
        })
        setCurrentIndex(-1)
    };
    const handleChange = (e) => {
        let valueType=e.target.value
        let name = e.target.name

        setInput({...input, [name] : valueType})
    }

  return (
    <form method="post" onSubmit={handleSubmit} style={{color:"#fff"}} >
        <h1 className="judul" style={{color:"#fff"}} >Movie Form</h1>
        <label>Title :</label>
        <Input type="text" name="title" value={input.title} onChange={handleChange} placeholder=" Masukan title" required/>
        <br/><br/>

        <label>Genre :</label>
        <Input type="text" name="genre" value={input.genre} onChange={handleChange} placeholder="genre" required/>
        <br/><br/>

        <label>Description :</label>
        <TextArea type="text" name="description" value={input.description} onChange={handleChange} placeholder="description" required/>
        <br/><br/>

        <label>Review :</label>
        <TextArea type="text" name="review" value={input.review} onChange={handleChange} placeholder="review" required/>
        <br/><br/>

        <label>Duration :</label>
        <Input type="number" name="duration" value={input.duration} min={0} onChange={handleChange} placeholder="0" required/>
        <br/><br/>

        <label>Image url :</label>
        <TextArea type="text" name="image_url" value={input.image_url} onChange={handleChange} placeholder="image url" required/>
        <br/><br/>

        <label>Rating :</label>
        <Input type="number" name="rating" value={input.rating} min={0} max={10} onChange={handleChange} placeholder="0" required/>
        <br/><br/>

        <label>Year :</label>
        <Input type="number" name="year" value={input.year} min={1980} max={2021} onChange={handleChange} placeholder="0" required/>
        <br/><br/>

        <input className="btn-submit" type="submit"/>
    </form>
  );
};

export default MovieForm
