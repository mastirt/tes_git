import React, { useContext, useEffect, useState } from 'react'
import { Table,Button,Input,Space } from 'antd';
import { EditOutlined,DeleteOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { MovieContext } from '../../context/MovieContext';
import axios from 'axios';
import { AppContext } from '../../context/AppContext';

const MovieList = () => {

    const {Moviefunctions} = useContext(MovieContext)
    let {functionDelete} = Moviefunctions
    const {Appfunctions} = useContext(AppContext)
    let {handleText} = Appfunctions
    const { Search } = Input;
    let history = useHistory()
    const [filter,setFilter] = useState({
      genre:"",
      year:0,
      rating:0
    })
    const [dataMovie,setDataMovie] = useState([])
    const [fetchStatus,setFetchStatus] = useState(true)

    useEffect(()=>{

      const fetchDataMovie = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data
        setDataMovie(
            data.map((e)=>{
                let {
                    description,
                    duration,
                    genre,
                    id,
                    image_url,
                    rating,
                    review,
                    title,
                    year
                } =  e
                let images = handleText(image_url,20)
                let descriptions = handleText(description,30)
                let reviews = handleText(review,30)
                return {
                    descriptions,
                    duration,
                    genre,
                    id,
                    images,
                    rating,
                    reviews,
                    title,
                    year
                }
            })
        )
    }

        if(fetchStatus){
            fetchDataMovie()
            setFetchStatus(false)
        }

    },[fetchStatus,setFetchStatus])

    const handleEdit = (event) => {
        let idData = parseInt(event.currentTarget.value)
        history.push(`/Movies/Form/${idData}`)
    }
    const handleDelete = (event) => {
        let idData = parseInt(event.currentTarget.value)
        functionDelete(idData)
    }

    const columns = [
        {
          title: 'Title',
          dataIndex: 'title',
          key: 'title',
        },
        {
          title: 'Year',
          dataIndex: 'year',
          key: 'year',
        },
        {
          title: 'Genre',
          dataIndex: 'genre',
          key: 'genre',
        },
        {
          title: 'Duration',
          dataIndex: 'duration',
          key: 'duration',
        },
        {
          title: 'Image',
          key: 'images',
          dataIndex: 'images',
        },
        {
          title: 'Rating',
          key: 'rating',
          dataIndex: 'rating',
        },
        {
          title: 'Description',
          key: 'descriptions',
          dataIndex: 'descriptions',
        },
        {
          title: 'Review',
          key: 'reviews',
          dataIndex: 'reviews',
        },
        {
          title: 'Action',
          key: 'action',
          render: (res, index) => (
            <>
                <Button onClick={handleEdit} value={res.id} ><EditOutlined /></Button>
                <Button onClick={handleDelete} value={res.id} ><DeleteOutlined /></Button>
            </>
          ),
        },
      ];
      
    const data = dataMovie

    const onSearch = value => {
      let fetchSearch = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data

        let searchResult = data.filter((e)=>{
          return Object.values(e).join("").toLowerCase().includes(value.toLowerCase())
        })

        setDataMovie(
          searchResult.map((e)=>{
              let {
                  description,
                  duration,
                  genre,
                  id,
                  image_url,
                  rating,
                  review,
                  title,
                  year
              } =  e
              let images = handleText(image_url,20)
              let descriptions = handleText(description,30)
              let reviews = handleText(review,30)
              return {
                  descriptions,
                  duration,
                  genre,
                  id,
                  images,
                  rating,
                  reviews,
                  title,
                  year
              }
          })
        )
      }  

      fetchSearch()
      
  }

  const handleFilter = (event) => {
    event.preventDefault()
    console.log(filter)

    let fetchFilter = async() =>{
      let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
      let data = result.data
      
      let filterResult = data.filter((e)=>{
        return e.genre === filter.genre || e.year === filter.year || e.rating === filter.rating
      })

      setDataMovie(
        filterResult.map((e)=>{
            let {
                description,
                duration,
                genre,
                id,
                image_url,
                rating,
                review,
                title,
                year
            } =  e
            let images = handleText(image_url,20)
            let descriptions = handleText(description,30)
            let reviews = handleText(review,30)
            return {
                descriptions,
                duration,
                genre,
                id,
                images,
                rating,
                reviews,
                title,
                year
            }
        })
      )
    }
    fetchFilter()
  }

  const handleChangeFilter = (e) =>{
    let valueType = e.target.value
    let name = e.target.name

    setFilter({...filter, [name]:valueType})
  }

    return (
        <>
            <label style={{margin: "0 auto", color:"white"}}>Filter Movie</label>
            <form onSubmit={handleFilter} >
              <div style={{display:"flex", width:"100%", justifyContent:"space-between",alignItems:"center",marginTop:"10px"}} >
                <Input onChange={handleChangeFilter}  value={filter.genre} name="genre" placeholder="Genre" style={{width:"33.3%"}} required/>
                <Input onChange={handleChangeFilter}  value={filter.year} name="year" type="number" min={1980} max={2021} placeholder="Year" style={{width:"33.3%"}} required/>
                <Input onChange={handleChangeFilter}  value={filter.rating} name="rating" type="number" min={0} max={10} placeholder="Rating" style={{width:"33.3%"}} required />
              </div>
              <input type="submit" style={{width:"100%",height:"40px", background:"#164c7e", color:"white"}} value="Filter"/>
            </form>
              <button onClick={() => {
                setFetchStatus(true)
                setFilter({
                  genre:"",
                  release:0,
                  platform:""
                })
              }} style={{width:"100%",height:"40px", background:"#164c7e", color:"white",marginTop:"10px"}}>Reset Filter</button>
            <br/>
            <label style={{margin: "0 auto", color:"white"}}>Search Movie</label><br/>
            <Space direction="vertical">
              <Search placeholder="Search Movie" onSearch={onSearch} enterButton />
            </Space>
            <br/><br/>
            <Table columns={columns} dataSource={data} />
        </>
    )
}

export default MovieList
