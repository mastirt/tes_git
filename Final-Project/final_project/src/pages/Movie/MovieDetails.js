import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Typography } from 'antd';
import { ClockCircleOutlined,StarFilled } from '@ant-design/icons';

const { Title, Paragraph } = Typography;

const MovieDetails = () => {
    let {Id} = useParams()

    const [dataMovie,setDataMovie] = useState([])

    useEffect(()=>{
        if (Id !== undefined){
            let fetchMovieById = async() => {
                let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${Id}`)
                console.log(result)
                let data = result.data
                let {
                    description,
                    duration,
                    genre,
                    id,
                    image_url,
                    rating,
                    review,
                    title,
                    year,
                } = data

                console.log(data)

                setDataMovie({
                    description,
                    duration,
                    genre,
                    id,
                    image_url,
                    rating,
                    review,
                    title,
                    year,
                })
            }

            fetchMovieById()
        }
    },[])
    console.log(dataMovie)
    return (
        <>
            <div className="container-detail">
                <img src={dataMovie.image_url} alt="Picture" className="img-detail" />
                <div>
                    <Typography style={{color:"#fff"}}>
                        <Title style={{color:"#fff"}}>{dataMovie.title} ({dataMovie.year})</Title>
                        <medium><ClockCircleOutlined />{dataMovie.duration} Minutes</medium><br/>
                        <medium><StarFilled />{dataMovie.rating}/10 Stars</medium><br/>
                        <medium>Genre : {dataMovie.genre}</medium><br/><br/>
                        <Paragraph style={{color:"#fff"}}>
                            {dataMovie.description}
                        </Paragraph>
                    </Typography>
                </div>
            </div>
        </>
    )
}

export default MovieDetails

