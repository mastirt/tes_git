import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Typography } from 'antd';
import { AppstoreFilled,IdcardFilled } from '@ant-design/icons';

const { Title } = Typography;

const GameDetails = () => {
    let {Id} = useParams()

    const [dataGame,setDataGame] = useState([])

    useEffect(()=>{
        if (Id !== undefined){
            let fetchGameById = async() => {
                let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${Id}`)
                console.log(result)
                let data = result.data
                let {
                    genre,
                    id,
                    image_url, 
                    multiplayer, 
                    name, 
                    platform,
                    release, 
                    singlePlayer,
                } = data

                console.log(data)

                setDataGame({
                    genre,
                    id,
                    image_url, 
                    multiplayer, 
                    name, 
                    platform,
                    release, 
                    singlePlayer,
                })
            }

            fetchGameById()
        }
    },[])
    console.log(dataGame)
    return (
        <>
            <div className="container-detail">
                <img src={dataGame.image_url} alt="Picture" className="img-detail" />
                <div>
                    <Typography style={{color:"#fff"}}>
                        <Title style={{color:"#fff"}}>{dataGame.name} ({dataGame.release})</Title>
                        <medium><AppstoreFilled /> {dataGame.platform}</medium><br/>
                        <medium><IdcardFilled /> {dataGame.singlePlayer === 1 ? "Singeplayer" : null}{dataGame.singlePlayer === dataGame.multiplayer ? " & " : null}{dataGame.multiplayer === 1 ? "Multiplayer":null}</medium><br/>
                        <medium>Genre : {dataGame.genre}</medium><br/><br/>
                    </Typography>
                </div>
            </div>
        </>
    )
}

export default GameDetails

