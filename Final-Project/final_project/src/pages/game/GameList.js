import React, { useContext, useEffect, useState } from 'react'
import { Table,Button,Input, Space } from 'antd';
import { EditOutlined,DeleteOutlined } from '@ant-design/icons';
import { GameContext } from '../../context/GameContext';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { AppContext } from '../../context/AppContext';

const GameList = () => {

    const {functions} = useContext(GameContext)
    let {functionDelete} = functions
    let history = useHistory()
    const {Appfunctions} = useContext(AppContext)
    let {handleText} = Appfunctions
    const [filter,setFilter] = useState({
      genre:"",
      release:0,
      platform:""
    })
    const [dataGame,setDataGame] = useState([])
    const [fetchStatus,setFetchStatus] = useState(true)

    const { Search } = Input;

    useEffect(()=>{

      const fetchData = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data
        setDataGame(
            data.map((e)=>{
                let {
                    genre,
                    id,
                    image_url, 
                    multiplayer, 
                    name, 
                    platform,
                    release, 
                    singlePlayer
                } =  e
                let images = handleText(image_url,20)
                return {
                    genre,
                    id,
                    images, 
                    multiplayer, 
                    name, 
                    platform,
                    release, 
                    singlePlayer
                }
            })
        )
    }

        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }

    },[fetchStatus,setFetchStatus])

    const handleEdit = (event) => {
        let idData = parseInt(event.currentTarget.value)
        history.push(`/Games/Form/${idData}`)
    }
    const handleDelete = (event) => {
        let idData = parseInt(event.currentTarget.value)
        functionDelete(idData)
    }

    const onSearch = value => {  

      let fetchSearch = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data

        let searchResult = data.filter((e)=>{
          return Object.values(e).join("").toLowerCase().includes(value.toLowerCase())
        })

        setDataGame(
          searchResult.map((e)=>{
              let {
                  genre,
                  id,
                  image_url, 
                  multiplayer, 
                  name, 
                  platform,
                  release, 
                  singlePlayer
              } =  e
              let images = handleText(image_url,20)
              return {
                  genre,
                  id,
                  images, 
                  multiplayer, 
                  name, 
                  platform,
                  release, 
                  singlePlayer
              }
          })
        )
      }

        fetchSearch()
        
    }

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Release',
          dataIndex: 'release',
          key: 'release',
        },
        {
          title: 'Genre',
          dataIndex: 'genre',
          key: 'genre',
        },
        {
          title: 'Platform',
          dataIndex: 'platform',
          key: 'platform',
        },
        {
          title: 'Image',
          key: 'images',
          dataIndex: 'images',
        },
        {
          title: 'SinglePlayer',
          key: 'singlePlayer',
          dataIndex: 'singlePlayer',
        },
        {
          title: 'Multiplayer',
          key: 'multiplayer',
          dataIndex: 'multiplayer',
        },
        {
          title: 'Action',
          key: 'action',
          render: (res, index) => (
            <>
                <Button onClick={handleEdit} value={res.id} ><EditOutlined /></Button>
                <Button onClick={handleDelete} value={res.id} ><DeleteOutlined /></Button>
            </>
          ),
        },
      ];
      
    const data = dataGame

    const handleFilter = (event) => {
      event.preventDefault()
      console.log(filter)

      let fetchFilter = async() =>{
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data
        
        let filterResult = data.filter((e)=>{
          return e.genre === filter.genre || e.release === filter.release || e.platform === filter.platform
        })

        setDataGame(
          filterResult.map((e)=>{
              let {
                  genre,
                  id,
                  image_url, 
                  multiplayer, 
                  name, 
                  platform,
                  release, 
                  singlePlayer
              } =  e
              let images = handleText(image_url,20)
              return {
                  genre,
                  id,
                  images, 
                  multiplayer, 
                  name, 
                  platform,
                  release, 
                  singlePlayer
              }
          })
        )
      }
      fetchFilter()
    }

    const handleChangeFilter = (e) =>{
      let valueType = e.target.value
      let name = e.target.name

      setFilter({...filter, [name]:valueType})
    }

    return (
        <>  
            <label style={{margin: "0 auto", color:"white"}}>Filter Game</label>
            <form onSubmit={handleFilter} >
              <div style={{display:"flex", width:"100%", justifyContent:"space-between",alignItems:"center",marginTop:"10px"}} >
                <Input onChange={handleChangeFilter}  value={filter.genre} name="genre" placeholder="Genre" style={{width:"33.3%"}} required/>
                <Input onChange={handleChangeFilter}  value={filter.release} name="release" type="number" min={2000} max={2021} placeholder="Release" style={{width:"33.3%"}} required/>
                <Input onChange={handleChangeFilter}  value={filter.platform} name="platform" placeholder="Platform" style={{width:"33.3%"}} required />
              </div>
              <input type="submit" style={{width:"100%",height:"40px", background:"#164c7e", color:"white"}} value="Filter"/>
            </form>
              <button onClick={() => {
                setFetchStatus(true)
                setFilter({
                  genre:"",
                  release:0,
                  platform:""
                })
              }} style={{width:"100%",height:"40px", background:"#164c7e", color:"white",marginTop:"10px"}}>Reset Filter</button>
            <br/>
            <label style={{margin: "0 auto", color:"white"}}>Search Game</label><br/>
            <Space direction="vertical">
              <Search placeholder="search game" onSearch={onSearch} enterButton />
            </Space>
            <br/><br/>
            <Table columns={columns} dataSource={data} />
        </>
    )
}

export default GameList
