import { Input } from 'antd';
import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import { GameContext } from '../../context/GameContext';

const { TextArea } = Input;

const GameForm = () => {


    const {states,functions} = useContext(GameContext)
    let {input,setInput,currentIndex,setCurrentIndex}= states
    let {fetchById,functionUpdate,
        functionSubmit} = functions
    let {value} = useParams()

    useEffect(()=>{

        if(value!==undefined){
            fetchById(value)
        }

    },[])

    const handleSubmit = (event) => {
        event.preventDefault()
        if(currentIndex === -1){
            functionSubmit(currentIndex)
        }else{
            functionUpdate(currentIndex)
        }
        setInput({
            genre: "",
            image_url: "",
            singlePlayer:  false,
            multiplayer:  false,
            name: "",
            platform: "",
            release:""
        })
        setCurrentIndex(-1)
    };
    const handleChange = (e) => {
        let valueType=e.target.value
        let name = e.target.name
        let platform = ["singlePlayer","multiplayer"]

        if(platform.indexOf(name)=== -1){
            setInput({...input,[name]:valueType})
        }else{
            setInput({...input,[name] : !input[name]})
        }
    }

  return (
    <form method="post" onSubmit={handleSubmit} style={{color:"#fff"}} >
        <h1 className="judul" style={{color:"#fff"}} >Game Form</h1>
            <label>Nama :</label>
            <Input type="text" name="name" value={input.name} onChange={handleChange} placeholder=" Masukan Nama Game" required /><br/><br/>
            <label>Genre :</label>
            <Input type="text" name="genre" value={input.genre} onChange={handleChange} placeholder="genre" required /><br/><br/>
            <label>Platform :</label>
            <Input type="text" name="platform" value={input.platform} onChange={handleChange} placeholder="platform" required /><br/><br/>
            <label>Image url :</label>
            <TextArea type="text" name="image_url" value={input.image_url} onChange={handleChange} placeholder="image url" required /><br/><br/>
            <label>Release :</label>
            <Input type="number" name="release" value={input.release} min={2000} max={2021} onChange={handleChange} placeholder="0" required /><br/><br/>
        <div>
            <label>Singleplayer : </label>
            <input onChange={handleChange} type="checkbox" checked={input.singlePlayer} name="singlePlayer" />
        </div>
        <div>
            <label>Multiplayer : </label>
            <input onChange={handleChange} type="checkbox" checked={input.multiplayer} name="multiplayer" />
        </div>
        <input className="btn-submit" type="submit" />
    </form>
  );
};

export default GameForm
