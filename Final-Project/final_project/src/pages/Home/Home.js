import React, { useContext, useEffect, useState } from 'react'
import { Card,Divider } from 'antd';
import axios from 'axios'
import { Link } from 'react-router-dom';
import { AppContext } from '../../context/AppContext';

const { Meta } = Card;

const Home = () => {

    const {Appfunctions} = useContext(AppContext)

    let { handleText } = Appfunctions

    const [dataMovie,setDataMovie] = useState([])
    const [dataGame,setDataGame] = useState([])
    const [fetchStatus,setFetchStatus] = useState(true)

    const CardComponent = (props) =>{
        return (
            <>
                <Link to={`/${props.dataName}/${props.id}`}>
                    <Card
                        hoverable
                        style={props.dataName === "Game" ?{ width: 240, height:350 } : { width: 240, height:450 } }
                        cover={<img alt="example" src={props.img} style={{height:"300px", objectFit:"cover"}} />}
                    >
                        <Meta title={props.title} description={ props.dataName === "Game" ? props.description : handleText(props.description,100)} />
                    </Card>
                </Link>
            </>
        )
    }

    useEffect(()=>{
        let fetchMovie =  async () => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
            let data = result.data
            setDataMovie(
                data.map((e)=>{
                    let {
                        description,
                        duration,
                        genre,
                        id,
                        image_url,
                        rating,
                        review,
                        title,
                        year
                    } = e
                    return{
                        description,
                        duration,
                        genre,
                        id,
                        image_url,
                        rating,
                        review,
                        title,
                        year
                    }
                })
            )
        }
        let fetchGame = async() => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            let data = result.data
            setDataGame(
                data.map((e)=>{
                    let {
                        genre,
                        id,
                        image_url, 
                        multiplayer, 
                        name, 
                        platform,
                        release, 
                        singlePlayer
                    } =  e
                    return {
                        genre,
                        id,
                        image_url, 
                        multiplayer, 
                        name, 
                        platform,
                        release, 
                        singlePlayer
                    }
                })
            )
        }
        if(fetchStatus) {
            fetchMovie()
            fetchGame()
            setFetchStatus(false)
        }

    },[fetchStatus,setFetchStatus])

    return (
        <>
            <Divider plain style={{fontSize:35,fontWeight:"bold",marginTop:-10,color:"#fff"}}>Movie</Divider>
            <div className="container-home">
                {
                    dataMovie !== null && (
                        <>
                            {dataMovie.filter((e,index)=> {
                                return index < 4
                            }).map((e,index)=>{
                                return <CardComponent key={index} dataName="Movie" id={e.id} img={e.image_url} name={e.title} description={handleText(e.description,100)} />
                            })}
                        </>
                    )
                }
            </div>
            <Divider plain style={{fontSize:35,fontWeight:"bold",color:"#fff"}}>Game</Divider>
            <div className="container-home">
                {
                    dataGame !== null && (
                        <>
                            {dataGame.filter((e,index)=> {
                                return index < 4
                            }).map((e,index)=>{
                                return <CardComponent key={index} dataName="Game" id={e.id} img={e.image_url} name={e.name} description={e.platform} />
                            })}
                        </>
                    )
                }
            </div>
        </>
    )
}

export default Home
