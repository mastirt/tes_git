import { Form, Input, Button, Checkbox } from 'antd';
import React,{ useState } from 'react';
import axios from 'axios'
import Cookies from "js-cookie"
import { useHistory } from 'react-router-dom';

const Login = () => {

    let history = useHistory()

    const [input,setInput] = useState({
        email:"",
        password:""
    })

  const onFinish = (event) => {
    console.log(event);

    axios.post(`https://backendexample.sanbersy.com/api/user-login`,{
        email: event.email,
        password: event.password
    }).then((e)=>{
        alert("Login Berhasil")
        let token =  e.data.token
        Cookies.set('token',token, {expires:1})
        history.push('/')
    }).catch((e)=>{
        alert(e)
    })
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
        <h1 style={{color:"#fff",paddingLeft:50,fontSize:40}}>Login</h1>
        <Form
        name="basic"
        className="form"
        labelCol={{
            span: 2,
        }}
        wrapperCol={{
            span: 16,
        }}
        initialValues={{
            remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        method="post"
        >
        <Form.Item
            label="email"
            name="email"
            rules={[
            {
                required: true,
                message: 'Please input your email!',
            },
            ]}
        >
            <Input value={input.email} />
        </Form.Item>

        <Form.Item
            label="Password"
            name="password"
            rules={[
            {
                required: true,
                message: 'Please input your password!',
            },
            ]}
        >
            <Input.Password value={input.password} />
        </Form.Item>

        <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
            offset: 2,
            span: 16,
            }}
        >
            <Checkbox style={{color:"white"}}>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
            wrapperCol={{
            offset: 2,
            span: 16,
            }}
        >
            <Button type="primary" htmlType="submit">
            Submit
            </Button>
        </Form.Item>
        </Form>
    </>
  );
};

export default Login
