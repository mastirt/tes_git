import { Form, Input, Button, Checkbox } from 'antd';
import React from 'react';
import axios from 'axios'
import Cookies from "js-cookie"
import { useHistory } from 'react-router-dom';

const ChangePassword = () => {

    let history = useHistory()

  const onFinish = (event) => {
    console.log(event);

    axios.post(`https://backendexample.sanbersy.com/api/change-password`,{
        current_password: event.current_password,
        new_password : event.new_password ,
        new_confirm_password: event.new_confirm_password
    },{
        headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
    }).then((e)=>{
        alert("Change Password Berhasil")
        history.push('/')
    }).catch((e)=>{
        alert(e.response.data)
    })
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
      <>
        <h1 style={{color:"#fff",paddingLeft:50,fontSize:40}}>Change Password</h1>
        <Form
        name="basic"
        labelCol={{
            span: 4,
        }}
        wrapperCol={{
            span: 16,
        }}
        initialValues={{
            remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        method="post"
        >
        <Form.Item
            label="Current Password"
            name="current_password"
            rules={[
            {
                required: true,
                message: 'Please input your current password!',
            },
            ]}
        >
            <Input.Password />
        </Form.Item>
        <Form.Item
            label="New Password"
            name="new_password"
            rules={[
            {
                required: true,
                message: 'Please input your new password !',
            },
            ]}
        >
            <Input.Password />
        </Form.Item>

        <Form.Item
            label="Confirm New Password"
            name="new_confirm_password"
            rules={[
            {
                required: true,
                message: 'Please input your new password!',
            },
            ]}
        >
            <Input.Password />
        </Form.Item>

        <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
            offset: 4,
            span: 16,
            }}
        >
            <Checkbox style={{color:"white"}}>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
            wrapperCol={{
            offset: 4,
            span: 16,
            }}
        >
            <Button type="primary" htmlType="submit">
            Submit
            </Button>
        </Form.Item>
        </Form>
      </>
  );
};

export default ChangePassword
