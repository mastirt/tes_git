import React from 'react'
import LayoutComponent from '../layout/Layout'
import Home from '../pages/Home/Home'
import Login from '../pages/login/Login'
import Register from '../pages/register/Register'
import MovieDetails from '../pages/Movie/MovieDetails'
import GameDetails from '../pages/game/GameDetails'
import GameForm from '../pages/game/GameForm'
import { GameProvider } from '../context/GameContext'
import { MovieProvider } from '../context/MovieContext'
import MovieList from '../pages/Movie/MovieList'
import MovieForm from '../pages/Movie/MovieForm'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
  } from "react-router-dom"
import Cookies from 'js-cookie'
import ChangePassword from '../pages/changePassword/ChangePassword'
import GameList from '../pages/game/GameList'

const Routes = () => {

    const RouteLogin = ({...props}) => {
        if(Cookies.get('token') === undefined){
            return <Route {...props}/>
        }else{
            return <Redirect to="/"/>
        }
    }
    const PrivateRoute = ({...props}) => {
        if(Cookies.get('token') === undefined){
            return <Redirect to="/Login"/>
        }else{
            return <Route {...props}/>
        }
    }

    return (
        <>
            <Router>
                <GameProvider>
                <MovieProvider>
                    <Switch>
                        <Route path="/" exact>
                            <LayoutComponent content={<Home/>} />
                        </Route>
                        <Route path="/Home" exact>
                            <LayoutComponent content={<Home/>} />
                        </Route>
                        <RouteLogin path="/Login" exact>
                            <LayoutComponent content={<Login/>} />
                        </RouteLogin>
                        <RouteLogin path="/Register" exact>
                            <LayoutComponent content={<Register/>} />
                        </RouteLogin>
                        <PrivateRoute path="/Change-password" exact>
                            <LayoutComponent content={<ChangePassword/>} />
                        </PrivateRoute>
                        <Route path="/Movie/:Id" exact>
                            <LayoutComponent content={<MovieDetails/>} />
                        </Route>
                        <Route path="/Game/:Id" exact>
                            <LayoutComponent content={<GameDetails/>} />
                        </Route>
                        <PrivateRoute path="/Games" exact>
                            <LayoutComponent content={<GameList/>} />
                        </PrivateRoute>
                        <PrivateRoute path="/Games/Form/:value" exact>
                            <LayoutComponent content={<GameForm/>} />
                        </PrivateRoute>
                        <PrivateRoute path="/Games/Form" exact>
                            <LayoutComponent content={<GameForm/>} />
                        </PrivateRoute>
                        <PrivateRoute path="/Movies" exact>
                            <LayoutComponent content={<MovieList/>} />
                        </PrivateRoute>
                        <PrivateRoute path="/Movies/Form/:value" exact>
                            <LayoutComponent content={<MovieForm/>} />
                        </PrivateRoute>
                        <PrivateRoute path="/Movies/Form" exact>
                            <LayoutComponent content={<MovieForm/>} />
                        </PrivateRoute>
                    </Switch>  
                </MovieProvider>    
                </GameProvider>
            </Router>
        </>
    )
}

export default Routes
