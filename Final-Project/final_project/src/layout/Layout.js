import React from 'react'
import { Layout } from 'antd';
import Navbar from '../component/Navbar';
import Sidebar from '../component/Sidebar';
import Cookies from 'js-cookie';

const LayoutComponent = (props) => {
    const { Content,Footer } = Layout;
    
    return (
        <>
            <Layout>
                <Navbar/>
                <Layout>
                {Cookies.get('token') !== undefined ? <Sidebar/> : null}
                <Layout style={{ padding: '0 24px 24px', background:"#164c7e" }}>
                    <Content
                    className="site-layout-background"
                    style={{
                        padding: 24,
                        marginTop: 20,
                        minHeight: 600,
                        background: "#112a45",
                    }}
                    >
                    {props.content}
                    </Content>
                </Layout>
                </Layout>
                <Footer style={{ textAlign: 'center',backgroundColor:"#111d2c",color:"white" }}>Mastirt ©2015 Created by Mahesa Tirta Panjalu</Footer>
            </Layout>
        </>
    )
}

export default LayoutComponent
