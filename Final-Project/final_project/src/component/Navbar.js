import React from 'react'
import { Layout, Menu } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';

const Navbar = () => {
    const { Header } = Layout;

    let history = useHistory()

    const handleLogout = () => {
        Cookies.remove('token')
        history.push('/Login')
    }

    return (
        <>
            <Header className="header">
                <div className="logo" />
                <Menu theme={"dark"} mode="horizontal" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>
                    {Cookies.get('token') !== undefined ? <>
                        <Menu.Item key="2"><Link to="/Games">Game</Link></Menu.Item>
                        <Menu.Item key="3"><Link to="/Movies">Movie</Link></Menu.Item>
                    </> : null}
                    {
                        Cookies.get('token') === undefined ? 
                        <>
                        <Menu.Item key="4" style={{position:"absolute", right:"10px"}} ><Link to="/Login">Login</Link></Menu.Item>
                        <Menu.Item key="5" style={{position:"absolute", right:"10px", marginRight:"74px"}} ><Link to="/Register">Register</Link></Menu.Item>
                        </> : null
                    }
                    {
                        Cookies.get('token') !== undefined ? <Menu.Item key="6" style={{position:"absolute", right:"10px"}} onClick={handleLogout} >Logout</Menu.Item> : null
                    }
                </Menu>
            </Header>
        </>
    )
}

export default Navbar
