import React from 'react'
import { Layout, Menu} from 'antd';
import { AndroidOutlined, LaptopOutlined, SettingOutlined, HomeOutlined } from '@ant-design/icons';
import { Link, useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';

const Sidebar = () => {
    const { SubMenu } = Menu;
    const { Sider } = Layout;

    let history = useHistory()

    const handleLogout = () => {
        Cookies.remove('token')
        history.push('/Login')
    }

    return (
        <>
            <Sider width={200} className="site-layout-background">
                <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%', borderRight: 0 }}
                >
                <Menu.Item key="sub1" icon={<HomeOutlined />} ><Link to="/Home">Home</Link></Menu.Item>
                <SubMenu key="sub2" icon={<AndroidOutlined />} title="Game">
                    <Menu.Item key="1"><Link to="/Games" >Game List</Link></Menu.Item>
                    <Menu.Item key="2"><Link to="/Games/Form" >Game Form</Link></Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" icon={<LaptopOutlined />} title="Movie">
                    <Menu.Item key="4"><Link to="/Movies" >Movie List</Link></Menu.Item>
                    <Menu.Item key="5"><Link to="/Movies/Form" >Movie Form</Link></Menu.Item>
                </SubMenu>
                <SubMenu key="sub4" icon={<SettingOutlined />} title="Settings">
                    <Menu.Item key="6"><Link to="/Change-password">Change Password</Link></Menu.Item>
                    <Menu.Item key="7" onClick={handleLogout} >Logout</Menu.Item>
                </SubMenu>
                </Menu>
            </Sider>
        </>
    )
}

export default Sidebar
