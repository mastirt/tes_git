import React, { createContext } from 'react'

export const AppContext = createContext()

export const AppProvider = props => {

    const handleText = (text,num) => {
        if(text === null){
            return " "
        }else{
            return text.slice(text,num) + "..."
        }
    }

    let Appfunctions = {
        handleText
    }

    return (
        <AppContext.Provider value={{
            Appfunctions
        }}>
            {props.children}
        </AppContext.Provider>
    )
}
