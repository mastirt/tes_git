import axios from 'axios'
import Cookies from 'js-cookie'
import React, { createContext, useContext, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { AppContext } from './AppContext'

export const GameContext = createContext()

export const GameProvider = props => {

    const {Appfunctions} = useContext(AppContext)
    let history = useHistory()
    let {handleText} = Appfunctions
    const [dataGame,setDataGame] = useState([])
    const [fetchStatus,setFetchStatus] = useState(true)
    const [input,setInput] = useState({
        genre: "",
        image_url: "",
        singlePlayer:  false,
        multiplayer:  false,
        name: "",
        platform: "",
        release:""
    })
    const [currentIndex,setCurrentIndex] = useState(-1)

    const fetchById = async (value) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-game/${value}`)
        .then((e)=>{
            let data = e.data
            let {
                genre,
                image_url, 
                multiplayer, 
                name, 
                platform,
                release, 
                singlePlayer
            } = data
            setInput({
                genre,
                image_url, 
                multiplayer, 
                name, 
                platform,
                release, 
                singlePlayer
            })
            setCurrentIndex(data.id)
        })
    }

    const functionDelete = (idData) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idData}`,{
            headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
        })
        .then(()=>{
            setFetchStatus(true)
        })
    }

    const fetchData = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data
        setDataGame(
            data.map((e)=>{
                let {
                    genre,
                    id,
                    image_url, 
                    multiplayer, 
                    name, 
                    platform,
                    release, 
                    singlePlayer
                } =  e
                let images = handleText(image_url,20)
                return {
                    genre,
                    id,
                    images, 
                    multiplayer, 
                    name, 
                    platform,
                    release, 
                    singlePlayer
                }
            })
        )
    }

    const functionSubmit = (params) => {
        axios.post(`https://backendexample.sanbersy.com/api/data-game`,{
            genre : input.genre,
            image_url : input.image_url, 
            multiplayer: input.multiplayer, 
            name:input.name, 
            platform:input.platform,
            release:input.release, 
            singlePlayer:input.singlePlayer
        },{
            headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
        }).then((res)=>{
            console.log(res)
            setFetchStatus(true)
            history.push("/Games")
        })
    }

    const functionUpdate = (value) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${value}`,{
            genre : input.genre,
            image_url : input.image_url, 
            multiplayer: input.multiplayer, 
            name:input.name, 
            platform:input.platform,
            release:input.release, 
            singlePlayer:input.singlePlayer
        },{
            headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
        }).then((e)=>{
            console.log(e)
            setFetchStatus(true) 
            history.push("/Games")      
        })
    }

    let states = {
        fetchStatus,setFetchStatus,
        dataGame,setDataGame,
        input,setInput,
        currentIndex,setCurrentIndex,
    }

    let functions = {
        fetchData,functionDelete
        ,fetchById,functionUpdate,
        functionSubmit
    }

    return (
        <GameContext.Provider value={{
            functions,states
        }}>
            {props.children}
        </GameContext.Provider>
    )
}
