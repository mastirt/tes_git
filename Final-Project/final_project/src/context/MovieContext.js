import axios from 'axios'
import Cookies from 'js-cookie'
import React, { createContext, useContext, useState } from 'react'
import { AppContext } from './AppContext'
import { useHistory } from 'react-router-dom';

export const MovieContext = createContext()

export const MovieProvider = props => {

    let history = useHistory()
    const {Appfunctions} = useContext(AppContext)
    let {handleText} = Appfunctions
    const [dataMovie,setDataMovie] = useState([])
    const [fetchStatus,setFetchStatus] = useState(true)
    const [input,setInput] = useState({
        description:"",
        duration:0,
        genre:"",
        image_url:"",
        rating:0,
        review:"",
        title:"",
        year:1980
    })
    const [currentIndex,setCurrentIndex] = useState(-1)

    const functionDelete = (idData) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idData}`,{
            headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
        })
        .then(()=>{
            setFetchStatus(true)
        })
    }

    const fetchById = async (value) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-movie/${value}`)
        .then((e)=>{
            let data = e.data
            let {
                description,
                duration,
                genre,
                id,
                image_url,
                rating,
                review,
                title,
                year
            } = data
            setInput({
                description,
                duration,
                genre,
                id,
                image_url,
                rating,
                review,
                title,
                year
            })
            setCurrentIndex(data.id)
        })
    }

    const fetchDataMovie = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data
        setDataMovie(
            data.map((e)=>{
                let {
                    description,
                    duration,
                    genre,
                    id,
                    image_url,
                    rating,
                    review,
                    title,
                    year
                } =  e
                let images = handleText(image_url,20)
                let descriptions = handleText(description,30)
                let reviews = handleText(review,30)
                return {
                    descriptions,
                    duration,
                    genre,
                    id,
                    images,
                    rating,
                    reviews,
                    title,
                    year
                }
            })
        )
    }

    const functionSubmit = (params) => {
        axios.post(`https://backendexample.sanbersy.com/api/data-movie`,{
            description:input.description,
            duration:input.duration,
            genre:input.genre,
            image_url:input.image_url,
            rating:input.rating,
            review:input.review,
            title:input.title,
            year:input.year
        },{
            headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
        }).then((res)=>{
            console.log(res)
            setFetchStatus(true)
            history.push("/Movies")
        })
    }

    const functionUpdate = (value) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-movie/${value}`,{
            description:input.description,
            duration:input.duration,
            genre:input.genre,
            image_url:input.image_url,
            rating:input.rating,
            review:input.review,
            title:input.title,
            year:input.year
        },{
            headers:{"Authorization" : "Bearer "+ Cookies.get('token')}
        }).then((e)=>{
            console.log(e)
            setFetchStatus(true) 
            history.push("/Movies")      
        })
    }

    let states = {
        fetchStatus,setFetchStatus,
        dataMovie,setDataMovie,
        input,setInput,
        currentIndex,setCurrentIndex
    }

    let Moviefunctions = {
        fetchDataMovie,functionDelete,
        fetchById,functionUpdate,
        functionSubmit
    }

    return (
        <MovieContext.Provider value={{
            Moviefunctions,states
        }}>
            {props.children}
        </MovieContext.Provider>
    )
}
