// SOAL 1
function luasPersegiPanjang(panjang, lebar) {
    return panjang*lebar
}
function kelilingPersegiPanjang(panjang, lebar) {
    return (panjang+lebar)*2
}
function volumeBalok(panjang, lebar, tiggi) {
    return panjang*lebar*tinggi
}

var panjang= 12
var lebar= 4
var tinggi = 8
 
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log('----SOAL 1----')
console.log(luasPersegiPanjang) 
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)

//SOAL 2
function introduce(name, age, address, hobby) {
    return 'Nama saya '+name+', umur saya '+age+' tahun, alamat saya di '+address+', dan saya punya hobby yaitu '+hobby
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log('----SOAL 2----')
console.log(perkenalan) 

// SOAl 3

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]

var objectDaftarPeserta ={
    nama : arrayDaftarPeserta[0],
    jenisKelamin :arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahunLahir : arrayDaftarPeserta[3]
}
console.log('----SOAL 3----')
console.log(objectDaftarPeserta);

// SOAL 4
var buah = [
    {nama : 'Nanas', warna : 'kuning', adaBijinya : false, harga:9000},
    {nama :'Jeruk', warna : 'oranye', adaBijinya : true, harga:9000},
    {nama : 'Semangka', warna : 'Hijau Dan Merah', adaBijinya : true, harga:10000},
    {nama : 'Pisang', warna : 'kuning', adaBijinya : false, harga:5000}
]

var buahfilter = buah.filter(function(item){
    return item.adaBijinya == false
})

console.log('----SOAL 4----')
console.log(buahfilter)

// SOAL 5
function tambahDataFilm(judul,waktu,genre,tahun) {
    var data = {}

    data.nama = judul;
    data.durasi = waktu;
    data.genre = genre;
    data.tahun = tahun;

    return data

}

var dataFilm = [
tambahDataFilm("LOTR", "2 jam", "action", "1999"),
tambahDataFilm("avenger", "2 jam", "action", "2019"),
tambahDataFilm("spiderman", "2 jam", "action", "2004"),
tambahDataFilm("juon", "2 jam", "horror", "2004")]
console.log('----SOAL 5----')
console.log(dataFilm)

var mobil = {
    nama : "ferrari",
    warna : "merah",
    tahun : 1971,
    detailMobil : function() {
    var res = "ferrari ini berwarna merah"
    return res;
    }
    }
    console.log(mobil.detailMobil);