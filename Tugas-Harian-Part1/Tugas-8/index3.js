var filterBooksPromise = require('./promise2.js')
 
// Lanjutkan code untuk menjalankan function filterBookPromise
var warna = true 
var hal = 40
function execute1 (warna, hal) {
    
    filterBooksPromise(warna,hal)
    .then( (res) => {
        console.log(res)
    })
    .catch( (err) => {
        console.log(err.message)
    })

}

const execute2 = async (color, page) => {
    try {
        let output = await filterBooksPromise(color,page)
        console.log(output)
    } catch (error) {
        console.log(error)
    }
}
const execute3 = async (color, page) => {
    try {
        let output = await filterBooksPromise(color,page)
        console.log(output)
    }catch (err) {
        console.log(err.message)
    }
}
execute1(warna,hal)
execute2(false,250)
execute3(true,30)