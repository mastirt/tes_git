var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
]
// Lanjutkan code untuk menjalankan function readBooksPromise 
var waktu = 10000
var index = 0
var x = 1
function execute (time) {
    
    readBooksPromise(time,books[index])
    .then( (res) => {
        if (x < books.length){
            x++
            index++
            execute(res)
        }
    })
    .catch( (err) => {
        console.log(err.message)
    })

}

execute(waktu)
