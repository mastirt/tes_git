// Soal 1
var kataPertama = 'saya';
var kataKedua = ' senang';
var kataKetiga = ' belajar';
var kataKeempat = ' javascript';

console.log(kataPertama.concat(kataKedua,kataKetiga,kataKeempat));

// soal 2
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var panjang = Number("8");
var lebar = Number("5");

var alas= Number("6");
var tinggi = Number("7");

var kelilingPersegiPanjang = 2*(panjang+lebar);
var luasSegitiga = 0.5*alas*tinggi ;

console.log(kelilingPersegiPanjang);
console.log(luasSegitiga);

// soal 3
var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substring(0, 3); 
var secondWord= sentences.substring(14, 4);; // do your own! 
var thirdWord= sentences.substring(14, 18);; // do your own! 
var fourthWord= sentences.substring(18, 24);; // do your own! 
var fifthWord= sentences.substring(24, 31);; // do your own! 

console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

//Soal 4
var nilaiJohn = 80;
var nilaiDoe = 50;

if (nilaiDoe >= 80) {
    console.log('predikat Doe A');
}else if( 80>nilaiDoe>=70 ){
    console.log('predikat Doe B');
}else if( 70>nilaiDoe>=60 ){
    console.log('predikat Doe C');
}else if( 60>nilaiDoe>=50 ){
    console.log('predikat Doe D');
}else{
    console.log('predikat Doe E');
}

if (nilaiJohn >= 80) {
    console.log('predikat John A');
}else if( 80>nilaiJohn>=70 ){
    console.log('predikat John B');
}else if( 70>nilaiJohn>=60 ){
    console.log('predikat John C');
}else if( 60>nilaiJohn>=50 ){
    console.log('predikat John D');
}else{
    console.log('predikat John E');
}

// soal 5
var tanggal = 22;
var bulan = 6;
var tahun = 2002;

switch(bulan) {
    case 1:   { var buln = 'Januari'; break; }
    case 2:   { var buln = 'Februari'; break; }
    case 3:   { var buln = 'Maret'; break; }
    case 4:   { var buln = 'April'; break; }
    case 5:   { var buln = 'Mei'; break; }
    case 6:   { var buln = 'Juni'; break; }
    case 7:   { var buln = 'Juli'; break; }
    case 8:   { var buln = 'Agustus'; break; }
    case 9:   { var buln = 'September'; break; }
    case 10:  { var buln = 'Oktober'; break; }
    case 11:  { var buln = 'November'; break; }
    case 12:  { var buln = 'Desember'; break; }
    default:  { var buln = 'Bulan lahir tidak tersedia'; }}

var tgl = String(tanggal);
var thn = String(tahun);

console.log(tgl + ' ' + buln +' '+ thn);