// SOAL 1
const luasLingkaran = (jari) => {
    const phi = 22/7

    return luas = phi * jari * jari
}
const kelilingLingkaran = (jari) => {
    const phi = 22/7

    return keliling = 2 * phi * jari
}
console.log('----SOAL1----')
console.log(luasLingkaran(7))
console.log(kelilingLingkaran(7))

// SOAL 2
const introduce = (...rest) => {
    const [nama, umur, kelamin,pekerjaan] = rest
    return `Pak ${nama} adalah Seorang ${pekerjaan} yang berusia ${umur} tahun`
}
console.log('----SOAL2----')

const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) 
// SOAL 3
const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
    }
}

console.log('----SOAL3----')
// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()
//   SOAL 4
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

const {brand : phoneBrand, name : phoneName, year, colors:[colorBronze, colorWhite, colorBlack]} = phone
console.log('----SOAL4----')

// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 

//  SOAL 5
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
    penulis: "john doe",
    tahunTerbit: 2020 
}

let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172,
    warnaSampul:["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

console.log('----SOAL5----')
let dataBuku = {...buku, warnaSampul:['hitam',...warna],...dataBukuTambahan}
console.log(dataBuku)