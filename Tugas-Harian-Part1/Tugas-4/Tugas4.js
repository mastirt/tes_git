// SOAL 1
var number = 0;
var angka =10 ;

console.log('LOOPING PERTAMA');
while (number < 10) {
    console.log('angka ke-'+number+ '- I love coding');
    number++;
}
console.log('LOOPING KEDUA');
while (angka >= 0) {
    console.log('angka ke-'+angka+'- I will become a frontend developer');
    angka--;
}
// SOAL 2
var x = 1 ;
for(x;x<=20;x++){
    var y = x%2;
    if ( y==0) {
        console.log(x+' - Berkualitas');
    } else {
        var z = x%3;
        if(z==0){
            console.log(x+' - I LOVE CODING');
        }else {
            console.log(x+' - Santai');
        }
    }
}
// SOAL 3
var x = '';
for (var a = 1 ; a <= 8 ; a++){
    x += ' ';
    for(var b =  1 ; b <= a ; b++){
        x +='*';
    }
    x += '\n'
}
console.log(x);
// SOAL 4
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.shift();
kalimat.splice(1,1);
var gabung = kalimat.join(' ');
console.log(gabung);
// SOAL 5
var sayuran=[]
sayuran.unshift('Kangkung','Bayam','Buncis','Kubis','Timun','Seledri','Tauge');
sayuran.sort();
var x = 1;
var y = 0;
while(x <= sayuran.length){
    console.log(x+'. '+sayuran[y]);
    y++;
    x++;
}

