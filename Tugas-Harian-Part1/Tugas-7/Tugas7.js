// SOAL 1
// Release 0
class Animal {
    // Code class di sini
    constructor (name){
        this.name = name
        this.kaki = 4
        this.cold_blooded = false
    }
    get legs(){
        return this.kaki
    }
    set legs(x){
        this.kaki = x
    }
}

var sheep = new Animal("shaun");

console.log('---- SOAL 1 -----')
console.log('---- RELEASE 0 -----')
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
sheep.legs = 3
console.log(sheep.legs)

// Release 1
class Ape extends Animal {
    constructor(name){
        super(name)
        
    }
    yell(){
        console.log('Auooo')
    }
}
class Frog extends Animal{
    constructor (name) {
        super(name)
    }
    jump(){
        console.log("hop hop")
    }
}

var sungokong = new Ape("kera sakti")
console.log('---- RELEASE 1 -----')
sungokong.yell() // "Auooo"
sungokong.legs = 2
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

// SOAL 2
class Clock {
    // Code di sini
    constructor (template) {
        this.timer;
        this.template = template
    }
    render() {
        var date = new Date();
        
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template 
    
        console.log(output = hours+':'+mins+':'+secs);
    }
    stop() {
        clearInterval(timer);
      };
    
    start () {
        this.render();
        this.timer = setInterval(this.render, 1000);
    };
  }
  console.log('-----SOAL 2-----')
  var clock = new Clock();
  clock.start(); 
