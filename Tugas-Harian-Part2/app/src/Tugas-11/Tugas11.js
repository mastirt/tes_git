import React,{useState} from 'react'

const Tugas11 = () => {
    const [daftarBuah,setDaftarBuah] = useState([
        {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
        {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
        {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
        {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
      ])
      const [inputBuah,setInputBuah] = useState({
          nama: "",
          hargaTotal: "",
          beratTotal: ""
      })
      const [currentIndex,setCurrentIndex] = useState(-1)

      const handleChange =(event)=> {
        let value = event.target.value
        let nameOfInput = event.target.name

        setInputBuah({...inputBuah, [nameOfInput] : value})

        // switch (name) {
        //     case "name":
        //         setInputBuah({...inputBuah, name : value })
        //         break;
        //     case "hargaTotal":
        //         setInputBuah({...inputBuah, hargaTotal : value })
        //         break;
        //     case "beratTotal":
        //         setInputBuah({...inputBuah, beratTotal : value })
        //         break;
        
        //     default:
        //         break;
        // }
      }

      const handleSubmit = (event)=> {
        event.preventDefault()
        let newData = daftarBuah


        if (currentIndex === -1) {
            newData = [...daftarBuah, {nama: inputBuah.nama ,hargaTotal: inputBuah.hargaTotal  ,beratTotal: inputBuah.beratTotal }]
        } else {
            newData[currentIndex] = {nama: inputBuah.nama ,hargaTotal: inputBuah.hargaTotal  ,beratTotal: inputBuah.beratTotal }
        }

        setDaftarBuah(newData)
        setInputBuah({
            nama: "",
            hargaTotal: "",
            beratTotal: ""
        })
      }

      const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = daftarBuah[index]
        let newData = daftarBuah.filter((e) => {return e !== deletedItem})
        setDaftarBuah(newData)
      }

      const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editValue = daftarBuah[index]
        setInputBuah(editValue)
        setCurrentIndex(event.target.value)
      }

    return (
        <div className="site-layout-content">
            <h1>Daftar Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Harga Total</th>
                        <th>Berat Total</th>
                        <th>Harga per Kg</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarBuah.map((val,index)=>{
                        return(
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{val.nama}</td>
                                <td>Rp.{val.hargaTotal}</td>
                                <td>{val.beratTotal/1000} Kg</td>
                                <td>Rp.{val.hargaTotal/val.beratTotal*1000}</td>
                                <td>
                                    <button onClick={handleEdit} value={index}>Edit</button>
                                    <button onClick={handleDelete} value={index}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            
            <h1 className="judul">Form Daftar Harga Buah</h1>
            <div className="containers">
                <form className="form" method="post" onSubmit={handleSubmit}>
                    <label>Nama :</label>
                    <input className="teks" type="text" name="nama" value={inputBuah.nama} onChange={handleChange} placeholder=" Masukan nama buah"/><br/><br/>
                    <label>Harga Total :</label>
                    <input className="teks" type="number" name="hargaTotal" value={inputBuah.hargaTotal} min={0} onChange={handleChange} placeholder="0"/><br/><br/>
                    <label>Berat Total(dalam gram) :</label>
                    <input className="teks" type="number" name="beratTotal" value={inputBuah.beratTotal} min={0} onChange={handleChange} placeholder="0"/><br/><br/>
                    <input className="tomvol" type="submit"/>
                </form>
            </div>
        </div>
    )
}

export default Tugas11
