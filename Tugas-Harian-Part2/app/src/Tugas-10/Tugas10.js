import React,{useEffect,useState} from 'react'

const Tugas10 = () => {

    const [date, setDate] = useState(new Date)
    const [waktu, setWaktu] = useState(date.toLocaleTimeString())
    const [count, setCount] = useState(100)
    const [show, setShow] = useState(true)

    const hide = () => {
        if(count===0){
            setShow(false)
        }
        setDate(new Date)
        setWaktu(date.toLocaleTimeString())
        setCount(count-1)
    }

    useEffect(() => {
        const interval = setInterval(hide, 1000);
        return () => {
            clearInterval(interval)
        }
    },[count])
    return (
        <div class="site-layout-content">
            {show?
            <div>
                <h1 class="time">Now At - {waktu} </h1>
                <h4> Countdown : {count}</h4>
            </div>:null
            }
        </div>

    )
}

export default Tugas10
