import React from 'react'
import ListSiswa from './ListSiswa'
import { SiswaProvider } from './SiswaContext'
import FormSiswa from './FormSiswa'

const DaftarSiswa = () => {
    return (
        <SiswaProvider>
            <ListSiswa/>
            <FormSiswa/>
        </SiswaProvider>
    )
}

export default DaftarSiswa
