import React,{useEffect, useContext} from 'react'
import { SiswaContext } from './SiswaContext'

const ListSiswa = () => {

    const { daftarMahasiswa,setDaftarMahasiswa,
        inputMahasiswa,setInputMahasiswa,
        currentIndex,setCurrentIndex,
        functions } = useContext(SiswaContext)

        const { fetchData,functionDelete,functionSubmit,functionUpdate,functionEdit} = functions

    useEffect(() => {
        fetchData()
    },[])



    const nilaiIndex = (e) => {
        if (e >= 80) {
            return "A"
        }else if( 80 > e && e >= 70 ){
            return "B"
        }else if( 70 > e && e >= 60 ){
            return "C"
        }else if( 60 > e && e >= 50 ){
            return "D"
        }else{
            return "E"
        }
    }
    
    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        // console.log(index)
        functionDelete(index)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        // console.log(index)
        functionEdit(index)
    }
    return (
        <div className="site-layout-content">
            <h1>Daftar Nilai Mahasiswa</h1>
            <table>
                <thead>
                    <tr >
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Indeks Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarMahasiswa.map((val,index)=>{
                        return(
                            <tr key={val.id}>
                                <td>{index+1}</td>
                                <td>{val.name}</td>
                                <td>{val.course}</td>
                                <td>{val.score}</td>
                                <td>{nilaiIndex(val.score)}</td>
                                <td>
                                    <button onClick={handleEdit} value={val.id}>Edit</button>
                                    <button onClick={handleDelete} value={val.id}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            
        </div>
    )
}

export default ListSiswa
