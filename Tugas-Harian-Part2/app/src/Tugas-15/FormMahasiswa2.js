import React, { useContext, useEffect } from 'react'
import { SiswaContext } from '../Tugas-14/Tugas14/MahasiswaContext' 
import { Link, useParams } from 'react-router-dom'
import { message } from 'antd'

const FormMahasiswa2 = () => {

    const {inputMahasiswa,setInputMahasiswa,
        currentIndex,setCurrentIndex,
        functions } = useContext(SiswaContext)

    const {functionSubmit,functionUpdate,fetchById} = functions
    let {Value} = useParams()

    useEffect(() => {
        if( Value !== undefined ){
            fetchById(Value)
        }
    },[])

    const handleChange =(event)=> {
        let value = event.target.value
        let nameOfInput = event.target.name

        setInputMahasiswa({...inputMahasiswa, [nameOfInput] : value})       
    }

    const handleSubmit = (event)=> {
        event.preventDefault()

        if(currentIndex === -1){
            functionSubmit(currentIndex)
        }else{
            functionUpdate(currentIndex)
        }
        setInputMahasiswa({
            name: "",
            course : "",
            score : 0
        })
        message.success('Data berhasil ditambah');
        setCurrentIndex(-1)
    }


    return (
        <>
            <div className="site-layout-content">
                <h1 className="judul">Form Nilai Mahasiswa</h1>
                <form className="form" method="post" onSubmit={handleSubmit}>
                    <label>Nama :</label>
                    <input className="teks" type="text" name="name" value={inputMahasiswa.name} onChange={handleChange} placeholder=" Masukan Nama Anda"/><br/><br/>
                    <label>Mata Kuliah :</label>
                    <input className="teks" type="text" name="course" value={inputMahasiswa.course} onChange={handleChange} placeholder="mata Kuliah"/><br/><br/>
                    <label>Nilai :</label>
                    <input className="teks" type="number" name="score" value={inputMahasiswa.score} min={0} max={100} onChange={handleChange} placeholder="0"/><br/><br/>
                    <input className="tomvol" type="submit"/>
                </form>
                <Link  to={'/Tugas15'}>Kembali ke Tabel</Link>
            </div>
        </>
    )
}

export default FormMahasiswa2
