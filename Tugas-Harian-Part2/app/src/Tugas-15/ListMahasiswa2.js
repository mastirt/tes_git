import React,{useEffect, useContext} from 'react'
import { SiswaContext } from '../Tugas-14/Tugas14/MahasiswaContext'
import { useHistory } from 'react-router-dom'
import { Button,message, Table } from 'antd';
import { DeleteOutlined,EditOutlined } from '@ant-design/icons';

const ListMahasiswa2 = () => {
    
    const { daftarMahasiswa,setInputMahasiswa,functions,fetchStatus, setFetchStatus } = useContext(SiswaContext)
    
    const { fetchData,functionDelete,functionEdit} = functions

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Mata Kuliah',
          dataIndex: 'course',
          key: 'course',
        },
        {
          title: 'Nilai',
          dataIndex: 'score',
          key: 'score',
        },
        {
          title: 'Indeks Nilai',
          key: 'indexScore',
          dataIndex: 'indexScore',
        },
        {
          title: 'Action',
          key: 'action',
          render: (res, index) => (
            <div>
                <Button onClick={handleEdit} value={res.id} className="buttonedt" ><EditOutlined /></Button>
                <Button onClick={handleDelete} value={res.id} className="buttondlt" ><DeleteOutlined /></Button>
            </div>
          ),
        },
      ];
      
      const data = daftarMahasiswa


      useEffect(() => {

        if (fetchStatus === false) {
            fetchData()
            setFetchStatus(true)
        }

    }, [fetchData, fetchStatus, setFetchStatus])
    
    const handleDelete = (event) => {
        let index = parseInt(event.currentTarget.value)
        // console.log(index)
        functionDelete(index)
        message.success('Data Terhapus');
    }

    const handleEdit = (event) => {
        let index = parseInt(event.currentTarget.value)
        // console.log(index)
        functionEdit(index)
        history.push(`/Tugas15/Create/${index}`);

    }
    let history = useHistory()
    const handleClick = () => {
        history.push("/Tugas15/Create");
        setInputMahasiswa({
            name:"",
            course:"",
            score:""
        })
    }

    return (
        <div className="site-layout-content">
            <h1>Daftar Nilai Mahasiswa</h1>
            <button onClick={handleClick}>Buat Daftar Nilai Mahasiswa Baru</button>
            {/* <table>
                <thead>
                    <tr >
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Indeks Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarMahasiswa.map((val,index)=>{
                        return(
                            <tr key={val.id}>
                                <td>{index+1}</td>
                                <td>{val.name}</td>
                                <td>{val.course}</td>
                                <td>{val.score}</td>
                                <td>{nilaiIndex(val.score)}</td>
                                <td>
                                    <button onClick={handleEdit} value={val.id}>Edit</button>
                                    <button onClick={handleDelete} value={val.id}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table> */}
            <Table columns={columns} dataSource={data} />
            
        </div>
    )
}

export default ListMahasiswa2
