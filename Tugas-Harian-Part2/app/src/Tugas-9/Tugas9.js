import React from 'react'
import Logo from '../assets/img/logo.png'

const Belajar = (props) => {
    return props.mapel
  }
const Tugas9 = () => {
    return (
        <div class="container">
            <img src={Logo} width="100%"/>
            <div class="atas">
              <h1>THINGS TO DO</h1>
              <p>During Bootcamp in Jabarcodingcamp</p>
            </div>

            <form> 
              <div class="kotak">
                <input class="box" type="checkbox" id="git" name="git" value="GIT&CLI"/>
                <label for="git"> Belajar <Belajar mapel="Git & CLI"/></label>
              </div>
              <div class="kotak">
                <input class="box" type="checkbox" id="html" name="html" value="HTML&CSS"/>
                <label for="html"> Belajar <Belajar mapel="HTML & CSS"/> </label>
              </div>
              <div class="kotak">
                <input class="box" type="checkbox" id="js" name="js" value="Javascript"/>
                <label for="js"> Belajar <Belajar mapel="Javascript"/></label>
              </div>
              <div class="kotak">
                <input class="box" type="checkbox" id="git" name="git" value="Reactjs-Dasar"/>
                <label for="git"> Belajar <Belajar mapel="ReactJS Dasar"/></label>
              </div>
              <div class="kotak">
                <input class="box" type="checkbox" id="adv" name="adv" value="Reactjs-Advance"/>
                <label for="adv"> Belajar <Belajar mapel="ReactJS Advance"/></label>
              </div>
            </form>
            <div className="tombol">
              <button type="button">Send</button>
            </div>
      </div>
    )
}

export default Tugas9
