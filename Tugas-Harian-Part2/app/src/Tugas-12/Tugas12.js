import React,{useState,useEffect} from 'react'
import axios from 'axios'

const Tugas12 = () => {
    const [daftarMahasiswa,setDaftarMahasiswa] = useState([])
    const [inputMahasiswa,setInputMahasiswa] = useState({
        id: "",
        name:"",
        course:"",
        score:""
    })
    const [currentIndex,setCurrentIndex] = useState(-1)

    useEffect(() => {
        const fetchData = async () => {
            let result = await axios.get('http://backendexample.sanbercloud.com/api/student-scores')
            let data = result.data
            let output = data.map((e) => {
                return {
                    id : e.id,
                    name : e.name,
                    course : e.course,
                    score : e.score
                }
            })
            setDaftarMahasiswa(output)
            console.log(data)
        }
        fetchData()
    },[])

    const handleChange =(event)=> {
        let value = event.target.value
        let nameOfInput = event.target.name

        setInputMahasiswa({...inputMahasiswa, [nameOfInput] : value})       
    }

    const nilaiIndex = (e) => {
        if (e >= 80) {
            return "A"
        }else if( 80 > e && e >= 70 ){
            return "B"
        }else if( 70 > e && e >= 60 ){
            return "C"
        }else if( 60 > e && e >= 50 ){
            return "D"
        }else{
            return "E"
        }
    }
    

    const handleSubmit = (event)=> {
        event.preventDefault()

        let name = inputMahasiswa.name
        let course = inputMahasiswa.course
        let score = inputMahasiswa.score
        if(currentIndex === -1){
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`,{
                name,
                course,
                score
            })
            .then((res)=>{
                setDaftarMahasiswa([...daftarMahasiswa,{id:res.data.id, name:res.data.name, course:res.data.course, score:res.data.score}])
            })
        }else{
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, {
                name,
                course,
                score
            })
            .then((res)=>{
                let update = daftarMahasiswa.find((e)=> e.id === currentIndex)
                // console.log(update)
                update.name = name
                update.course = course
                update.score = score
                // update = {
                    
                // }
                setDaftarMahasiswa([...daftarMahasiswa])

            })
        }
        setInputMahasiswa({
            name: "",
            course : "",
            score : 0
        })
        setCurrentIndex(-1)
    }

    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        // console.log(index)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${index}`)
        .then(()=> {
            let newDaftarSiswa = daftarMahasiswa.filter((e) => {return e.id !== index})
            setDaftarMahasiswa(newDaftarSiswa)
        })
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        // console.log(index)
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${index}`)
        .then((res)=>{
            let data = res.data
            setInputMahasiswa({...inputMahasiswa,
                name: data.name,
                course : data.course,
                score : data.score
            })
            setCurrentIndex(data.id)
        })
    }
    return (
        <div className="site-layout-content">
            <h1>Daftar Nilai Mahasiswa</h1>
            <table>
                <thead>
                    <tr >
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Indeks Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarMahasiswa.map((val,index)=>{
                        return(
                            <tr key={val.id}>
                                <td>{index+1}</td>
                                <td>{val.name}</td>
                                <td>{val.course}</td>
                                <td>{val.score}</td>
                                <td>{nilaiIndex(val.score)}</td>
                                <td>
                                    <button onClick={handleEdit} value={val.id}>Edit</button>
                                    <button onClick={handleDelete} value={val.id}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            
            <h1 className="judul">Form Nilai Mahasiswa</h1>
            <div className="containers">
                <form className="form" method="post" onSubmit={handleSubmit}>
                    <label>Nama :</label>
                    <input className="teks" type="text" name="name" value={inputMahasiswa.name} onChange={handleChange} placeholder=" Masukan Nama Anda"/><br/><br/>
                    <label>Mata Kuliah :</label>
                    <input className="teks" type="text" name="course" value={inputMahasiswa.course} onChange={handleChange} placeholder="mata Kuliah"/><br/><br/>
                    <label>Nilai :</label>
                    <input className="teks" type="number" name="score" value={inputMahasiswa.score} min={0} max={100} onChange={handleChange} placeholder="0"/><br/><br/>
                    <input className="tomvol" type="submit"/>
                </form>
            </div>
        </div>
    )
}

export default Tugas12
