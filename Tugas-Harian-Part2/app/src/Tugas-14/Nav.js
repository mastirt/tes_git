import React, { useContext } from 'react'
import {Link} from "react-router-dom"
import ButtonTheme from './ButtonTheme'
import { ThemeContext } from './temacolorContext'
import { Layout, Menu } from 'antd';

const { Header, Content, Footer } = Layout;

const Nav = () => {

  const {tema,setTema} = useContext(ThemeContext)

  const style = tema === "Light" ? "Light" : "Dark"

    return (
        <div>

          {/* <Header>
            <div className={"logo"}/>
              <Menu  mode="horizontal" defaultSelectedKeys={['1']}>
                <Menu.Item key={"1"}><Link to={"/Tugas9"}>Tugas9</Link></Menu.Item>
                <Menu.Item key={"2"}><Link to="/Tugas10">Tugas10</Link></Menu.Item>
                <Menu.Item key={"3"}><Link to={"/Tugas11"}>Tugas11</Link></Menu.Item>
                <Menu.Item key={"4"}><Link to={"/Tugas12"}>Tugas12</Link></Menu.Item>
                <Menu.Item key={"5"}><Link to={"/Tugas13"}>Tugas13</Link></Menu.Item>
                <Menu.Item key={"6"}><Link to={"/Tugas14"}>Tugas14</Link></Menu.Item>
                <Menu.Item key={"7"}><Link to={"/Tugas15"}>Tugas15</Link></Menu.Item>
              </Menu>
          </Header> */}

          <nav className={`navbar ${style}`}>
              <ul>
                <li>
                  <Link to={"/Tugas9"}>Tugas9</Link>
                </li>
                <li>
                  <Link to="/Tugas10">Tugas10</Link>
                </li>
                <li>
                  <Link to="/Tugas11">Tugas11</Link>
                </li>
                <li>
                  <Link to="/Tugas12">Tugas12</Link>
                </li>
                <li>
                  <Link to="/Tugas13">Tugas13</Link>
                </li>
                <li>
                  <Link to="/Tugas14">Tugas14</Link>
                </li>
                <li>
                  <Link to="/Tugas15">Tugas15</Link>
                </li>
              </ul>
              <ButtonTheme/>
            </nav>
        </div>
    )
}

export default Nav
