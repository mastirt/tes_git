import React,{useEffect, useContext } from 'react'
import { SiswaContext } from './MahasiswaContext'
import { useHistory } from 'react-router-dom'


const ListMahasiswa = () => {

    const { daftarMahasiswa,setInputMahasiswa,functions,fetchStatus, setFetchStatus } = useContext(SiswaContext)

    const { fetchData,functionDelete,functionEdit,nilaiIndex} = functions

    useEffect(() => {

        if (fetchStatus === false) {
            fetchData()
            setFetchStatus(true)
        }

    }, [fetchData, fetchStatus, setFetchStatus])

    
    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        // console.log(index)
        functionDelete(index)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        // console.log(index)
        functionEdit(index)
        history.push(`/Tugas14/Create/${index}`);

    }
    let history = useHistory()
    const handleClick = () => {
        history.push("/Tugas14/Create");
        setInputMahasiswa({
            name:"",
            course:"",
            score:""
        })
    }

    return (
        <div className="site-layout-content">
            <h1>Daftar Nilai Mahasiswa</h1>
            <button onClick={handleClick}>Buat Daftar Nilai Mahasiswa Baru</button>
            <table>
                <thead>
                    <tr >
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Indeks Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarMahasiswa.map((val,index)=>{
                        return(
                            <tr key={val.id}>
                                <td>{index+1}</td>
                                <td>{val.name}</td>
                                <td>{val.course}</td>
                                <td>{val.score}</td>
                                <td>{nilaiIndex(val.score)}</td>
                                <td>
                                    <button onClick={handleEdit} value={val.id}>Edit</button>
                                    <button onClick={handleDelete} value={val.id}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            
        </div>
    )
}

export default ListMahasiswa
