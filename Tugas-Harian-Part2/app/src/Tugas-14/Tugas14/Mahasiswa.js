import React from 'react'
import ListMahasiswa from './ListMahasiswa'
import { MahasiswaProvider } from './MahasiswaContext' 

const Mahasiswa = () => {
    return (
        <MahasiswaProvider>
            <ListMahasiswa/>
        </MahasiswaProvider>
    )
}

export default Mahasiswa
