import axios from 'axios'
import React, { createContext,useState } from 'react'

export const SiswaContext = createContext()

export const MahasiswaProvider = props => {
    
    const [daftarMahasiswa,setDaftarMahasiswa] = useState([])
    const [inputMahasiswa,setInputMahasiswa] = useState({
        name:"",
        course:"",
        score:""
    })
    const [currentIndex,setCurrentIndex] = useState(-1)
    const [fetchStatus, setFetchStatus] = useState(false)

    const fetchData = async () => {
        let result = await axios.get('http://backendexample.sanbercloud.com/api/student-scores')
        let data = result.data
        let output = data.map((e,index) => {
            // eslint-disable-next-line no-undef
            let indexScore = nilaiIndex(e.score)
            return {
                no: index+1,
                id : e.id,
                name : e.name,
                course : e.course,
                score : e.score,
                indexScore : indexScore
            }
        })
        setDaftarMahasiswa(output)
        console.log(data)
    }

    const fetchById = async (idMahasiswa) => {
        let res = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
        let data = res.data
        setInputMahasiswa({
            id: data.id,
            nama: data.name,
            course: data.course,
            score: data.score
        })
        setCurrentIndex(data.id)
    }


    const functionDelete = (params) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${params}`)
        .then(()=> {
            let newDaftarSiswa = daftarMahasiswa.filter((e) => {return e.id !== params})
            setDaftarMahasiswa(newDaftarSiswa)
        })
        console.log(params)
    }
    const functionEdit = (params) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${params}`)
        .then((res)=>{
            let data = res.data
            setInputMahasiswa({...inputMahasiswa,
                name: data.name,
                course : data.course,
                score : data.score
            })
            setCurrentIndex(data.id)
        })
        console.log(params)
    }

    const functionSubmit = (params) => {
        let name = inputMahasiswa.name
        let course = inputMahasiswa.course
        let score = inputMahasiswa.score
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores`,{
                name,
                course,
                score
            })
            .then((res)=>{
                setDaftarMahasiswa([...daftarMahasiswa,{id:res.data.id, name:res.data.name, course:res.data.course, score:res.data.score}])
        })
    }

    const nilaiIndex = (score) => {
        if (score >= 80) {
            return "A"
        }else if( 80 > score && score >= 70 ){
            return "B"
        }else if( 70 > score && score >= 60 ){
            return "C"
        }else if( 60 > score && score >= 50 ){
            return "D"
        }else{
            return "E"
        }
    }

    const functionUpdate = (params) => {
        let name = inputMahasiswa.name
        let course = inputMahasiswa.course
        let score = inputMahasiswa.score
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, {
                name,
                course,
                score
            })
            .then((res)=>{
                let update = daftarMahasiswa.find((e)=> e.id === currentIndex)
                // console.log(update)
                update.name = name
                update.course = course
                update.score = score
                // update = {
                    
                // }
                setDaftarMahasiswa([...daftarMahasiswa])

        })
    }

    const functions =  {
        fetchData,
        fetchById,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
        nilaiIndex
    }
 

    return(
        <SiswaContext.Provider value={{
            daftarMahasiswa,
            setDaftarMahasiswa,
            inputMahasiswa,
            setInputMahasiswa,
            currentIndex,
            setCurrentIndex,
            fetchStatus, 
            setFetchStatus,
            functions
        }}>
            {props.children}
        </SiswaContext.Provider>
    )
}


