import React, { useContext, useEffect } from 'react'
import { SiswaContext } from './MahasiswaContext'
import { Link, useHistory, useParams } from 'react-router-dom'

const FormMahasiswa = () => {

    const {inputMahasiswa,setInputMahasiswa,
        currentIndex,setCurrentIndex,
        functions } = useContext(SiswaContext)

    const {functionSubmit,functionUpdate,fetchById} = functions
    let {Value} = useParams()

    useEffect(() => {
        if( Value !== undefined ){
            fetchById(Value)
        }
    },[])


    const handleChange =(event)=> {
        let value = event.target.value
        let nameOfInput = event.target.name

        setInputMahasiswa({...inputMahasiswa, [nameOfInput] : value})       
    }

    const handleSubmit = (event)=> {
        event.preventDefault()

        if(currentIndex === -1){
            functionSubmit(currentIndex)
        }else{
            functionUpdate(currentIndex)
        }
        setInputMahasiswa({
            name: "",
            course : "",
            score : 0
        })
        setCurrentIndex(-1)
    }


    return (
        <>
            <div className="site-layout-content">
                <h1 className="judul">Form Nilai Mahasiswa</h1>
                <form className="form" method="post" onSubmit={handleSubmit}>
                    <label>Nama :</label>
                    <input className="teks" type="text" name="name" value={inputMahasiswa.name} onChange={handleChange} placeholder=" Masukan Nama Anda"/><br/><br/>
                    <label>Mata Kuliah :</label>
                    <input className="teks" type="text" name="course" value={inputMahasiswa.course} onChange={handleChange} placeholder="mata Kuliah"/><br/><br/>
                    <label>Nilai :</label>
                    <input className="teks" type="number" name="score" value={inputMahasiswa.score} min={0} max={100} onChange={handleChange} placeholder="0"/><br/><br/>
                    <input className="tomvol" type="submit"/>
                </form>
                <Link  to={'/Tugas14'}>Kembali ke Tabel</Link>
            </div>
        </>
    )
}

export default FormMahasiswa
