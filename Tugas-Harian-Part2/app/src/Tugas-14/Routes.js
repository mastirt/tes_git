import React from 'react';
import Nav from './Nav';
import Tugas9 from '../Tugas-9/Tugas9';
import Tugas10 from '../Tugas-10/Tugas10';
import Tugas11 from '../Tugas-11/Tugas11';
import Tugas12 from '../Tugas-12/Tugas12';
import DaftarSiswa from '../Tugas-13/DaftarSiswa';
import FormMahasiswa from './Tugas14/FormMahasiswa';
import { MahasiswaProvider } from './Tugas14/MahasiswaContext' 
import { ThemeProvider } from './temacolorContext';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom"
import ListMahasiswa from './Tugas14/ListMahasiswa';
import ListMahasiswa2 from '../Tugas-15/ListMahasiswa2';
import FormMahasiswa2 from '../Tugas-15/FormMahasiswa2';
import { Layout } from 'antd';

const { Header, Content, Footer } = Layout;

const Routes= () => {

  return (
    <>
      {/* <DaftarSiswa/> */}
      <Router>
        <Layout className="layout">
          <ThemeProvider>
            <MahasiswaProvider>

                <div >
                  <Nav/>
                  <Content style={{ padding: '0 50px' }}>
                    <Switch>

                      <Route path="/" exact component={Tugas9}/>
                      <Route path="/Tugas9" exact component={Tugas9}/>
                      <Route path="/Tugas10" exact component={Tugas10}/>
                      <Route path="/Tugas11" exact component={Tugas11}/>
                      <Route path="/Tugas12" exact component={Tugas12}/>
                      <Route path="/Tugas13" exact component={DaftarSiswa}/>
                      <Route path="/Tugas14" exact component={ListMahasiswa}/>
                      <Route path="/Tugas14/create" exact component={FormMahasiswa}/>
                      <Route path="/Tugas14/create/:index" exact component={FormMahasiswa}/>
                      <Route path="/Tugas15" exact component={ListMahasiswa2}/>
                      <Route path="/Tugas15/create" exact component={FormMahasiswa2}/>
                      <Route path="/Tugas15/create/:index" exact component={FormMahasiswa2}/>

              

                    </Switch>
                  </Content>
                  <Footer style={{ textAlign: 'center' }}>Mastirt ©2015 Created by Mahesa Tirta Panjalu</Footer>
                </div>

            </MahasiswaProvider>
          </ThemeProvider>
        </Layout>
      </Router>
    </>
  );
}

export default Routes;
