import React, { useContext } from 'react'
import { ThemeContext } from './temacolorContext'
import { Switch } from 'antd';

const ButtonTheme = () => {

    const {tema,setTema} = useContext(ThemeContext)
    console.log(tema)

    const handleButton =() => {
        setTema(tema === "Dark" ? "Light" : "Dark")
    }

    return (
        <>
            <Switch checkedChildren="Light" unCheckedChildren="Dark" defaultChecked onClick={handleButton}/>
            {/* <button class="buttontheme" onClick={handleButton} >Change Navbar Theme</button> */}
        </>
    )
}

export default ButtonTheme
