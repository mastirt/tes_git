import React, { createContext, useState } from 'react'

export const ThemeContext = createContext()

export const ThemeProvider = props => {
    const [tema,setTema] = useState("Light")
    return(
        <ThemeContext.Provider value={{
            tema,setTema
        }}>
            {props.children}
        </ThemeContext.Provider>
    )
}
