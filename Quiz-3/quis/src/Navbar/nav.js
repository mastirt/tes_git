import React, { useContext } from 'react'
import Logo from '../assets/img/logo.png'
import {Link} from "react-router-dom"
import { MobilesContext } from '../Context/MobileContext'

const Nav= () => {

    const {setSearch} = useContext(MobilesContext)

    return (
        <div to={'/'} className="topnav">
            <Link>
                <img src={Logo} width="100px" />
            </Link>
            <Link to={"/Home"}>Home</Link>
            <Link to={"/Mobile-list"}>Mobile List</Link>
            <Link to={"/About"}>About</Link>
            <form>
                <input type="text" onChange={(e)=>{
                    setSearch(e.target.value)
                }} />
                <input type="submit" value="Cari" />
            </form>
        </div>
    )
}

export default Nav
