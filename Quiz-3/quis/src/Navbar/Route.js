import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom"
import Home from '../Home/Home'
import MobileList from '../Mobile/MobileList/MobileList'
import Abouts from '../About/About'
import MobileForm from '../Mobile/MobileForm/MobileForm'
import Nav from './nav'
import Footer from '../footer/footers'
import { MobileProvider } from '../Context/MobileContext'

const Routes = () => {
    return (
        <>
            <Router>
                <MobileProvider>
                    <Nav/>
                        <Switch>
                            <Route path="/" exact component={Home}/>
                            <Route path="/Home" exact component={Home}/>
                            <Route path="/Mobile-list" exact component={MobileList}/>
                            <Route path="/Mobile-list/Form" exact component={MobileForm}/>
                            <Route path="/Mobile-list/Form/:index" exact component={MobileForm}/>
                            <Route path="/About" exact component={Abouts}/>
                            <Route path="/search/:valueOfSearch" exact component={Abouts}/>
                        </Switch>
                        <Footer/>
                </MobileProvider>
            </Router>  
        </>
    )
}

export default Routes
