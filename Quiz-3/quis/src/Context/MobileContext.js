import axios from 'axios'
import React, { createContext,useState } from 'react'

export const MobilesContext = createContext()

export const MobileProvider = props => {
    const [daftarMobile,setDaftarMobile] = useState([])
    const [search,setSearch] = useState('')
    const [input,setInput] = useState({
        name : '',
        description : '',
        category: '',
        release_year:2007,
        size:0,
        price: 0,
        rating : 0,
        image_url : '',
        is_android_app : true,
        is_ios_app : true
    })
    const [currentIndex,setCurrentIndex] = useState(-1)
    const [fetchStatus, setFetchStatus] = useState(false)

    const fetchData = async () => {
        let result = await axios.get('http://backendexample.sanbercloud.com/api/mobile-apps')
        let data = result.data
        let output = data.map((e,index) => {
            let indexPrice = priceIndex(e.price)
            return {
                name : e.name,
                description : e.description,
                category: e.category,
                size: e.size,
                release_year: e.release_year,
                price: e.price,
                rating : e.rating,
                image_url : e.image_url,
                is_android_app : e.is_android_app,
                is_ios_app : e.is_ios_app
            }
        })
        setDaftarMobile(output)
        console.log(data)
    }
    const fetchById = async (idMobile)=>{
        let res = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idMobile}`)
        let data = res.data
        setInput({
            id : data.id,
            name : data.name,
            description : data.description,
            category: data.category,
            size: data.size,
            release_year: data.release_year,
            price: data.price,
            rating : data.rating,
            image_url : data.image_url,
            is_android_app : data.is_android_app,
            is_ios_app : data.is_ios_app
        })
    }
    const functionDelete = (params) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${params}`)
        .then(() => {
            let newMobile = daftarMobile.filter((e) => {return e.id !== params})
            setDaftarMobile(newMobile)
        })
    }
    const functionEdit = (params) => {
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${params}`)
        .then((res) => {
            let data = res.data
            setInput({...input,
                name : data.name,
                description : data.description,
                category: data.category,
                size: data.size,
                release_year: data.release_year,
                price: data.price,
                rating : data.rating,
                image_url : data.image_url,
                is_android_app : data.is_android_app,
                is_ios_app : data.is_ios_app
            })
            setCurrentIndex(data.id)
        })
    }
    const functionSubmit = () =>{
        let name = input.name
        let description = input.description
        let category = input.category
        let size = input.size
        let price = input.price
        let rating = input.rating
        let image_url = input.image_url
        let android = input.is_android_app
        let ios = input.is_ios_app
        axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`,{
            name,
            description,
            category,
            size,
            price,
            rating,
            image_url,
            release_year: input.release_year,
            is_android_app : android,
            is_ios_app : ios
        }).then((res) => {
            let data = res.data
            setDaftarMobile([...daftarMobile,{
                id : data.id,
                name : data.name,
                description : data.description,
                category: data.category,
                size: data.size,
                price: data.price,
                release_year: data.release_year,
                rating : data.rating,
                image_url : data.image_url,
                is_android_app : data.is_android_app,
                is_ios_app : data.is_ios_app}])
        })
    }
    const priceIndex = (price) => {
        if (price === 0) {
            return "FREE"
        } else {
            return price
        }
    }
    // const platform = () => [
    //     if ()
    // ]
    const functionUpdate = (currentIndex) => {
        axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps/${currentIndex}`,{
            name: input.name,
            description : input.description,
            category : input.category,
            size : input.size,
            price : input.price,
            rating : input.rating,
            release_year: input.release_year,
            image_url: input.image_url,
            is_android_app : input.is_android_app,
            is_ios_app : input.is_ios_app
        })
    }
    const functions = {
        fetchData,
        priceIndex,
        functionSubmit,
        functionUpdate,
        functionDelete,
        functionEdit,
        fetchById
    }

    return(
        <MobilesContext.Provider value={{
            daftarMobile,setDaftarMobile,
            input,setInput,
            currentIndex,setCurrentIndex,
            fetchStatus,setFetchStatus,
            search,setSearch,
            functions
        }}>
            {props.children}
        </MobilesContext.Provider>
    )
}