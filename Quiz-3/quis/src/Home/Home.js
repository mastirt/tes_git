import React, { useContext, useEffect } from 'react'
import { MobilesContext } from '../Context/MobileContext'


const Home = () => {

    const {daftarMobile,functions,search} = useContext(MobilesContext)

    const {fetchData,priceIndex} = functions

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div >
        <div className="row">
            <div className="section">
                <div className="card">
                    {daftarMobile.filter((val)=>{
                        if (search == ""){
                            return val
                        }else if (val.name.toLowerCase().includes(search.toLowerCase())){
                            return val
                        }
                    }).map((val,index)=>{
                        return(
                            <>
                                <h2>{val.name}</h2>
                                <h5>Release Year : {val.release_year}</h5>
                                <img className="fakeimg" style={{width: '50%', height: '300px', objectFit: 'cover'}} src={val.image_url} />
                                <br />
                                <br />
                                <div>
                                    <strong>Price: {priceIndex(val.price)}</strong><br />
                                    <strong>Rating: {val.rating}</strong><br />
                                    <strong>Size: {val.size/1000} Gb</strong><br />
                                    <strong style={{ marginRight: '10px' }}>Platform : {val.is_android_app}{val.is_ios_app}
                                    </strong>
                                    <br />
                                </div>
                                <p>
                                    <strong style={{ marginRight: '10px' }} >Description :</strong>
                                    {val.description}
                                </p>
                                <hr />
                            </>
                        )
                    })}
                </div>
            </div>
        </div>
    </div>

    )
}

export default Home
