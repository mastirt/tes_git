import React from 'react'

const Footer = () => {
    return (
        <>
            <footer>
                <h5>&copy; Quiz 3 ReactJS JabarCodingCamp</h5>
            </footer>
        </>
    )
}

export default Footer
