import './App.css';
import 'antd/dist/antd.css';
import Routes from './Navbar/Route';

function App() {
  return (
    <>
      <Routes/>
    </>
  );
}

export default App;
