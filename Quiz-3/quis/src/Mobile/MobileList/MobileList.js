import React, { useContext, useEffect } from 'react'
import { MobilesContext } from '../../Context/MobileContext'
import { Button,message,Table } from 'antd'
import { DeleteOutlined,EditOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';

const MobileList = () => {

    let history = useHistory()

    const {daftarMobile,setInput,functions,fetchStatus,setFetchStatus} = useContext(MobilesContext)

    const { fetchData,functionDelete, functionEdit} = functions 

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Category',
          dataIndex: 'category',
          key: 'category',
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
        },
        {
          title: 'Release Year',
          key: 'release_year',
          dataIndex: 'release_year',
        },
        {
          title: 'Size(Gb)',
          key: 'size',
          dataIndex: 'size',
        },
        {
          title: 'Price',
          key: 'indexPrice',
          dataIndex: 'indexPrice',
        },
        {
          title: 'Rating',
          key: 'rating',
          dataIndex: 'rating',
        },
        {
          title: 'Platform',
          key: 'release_year',
          dataIndex: 'release_year',
        },
        {
          title: 'Action',
          key: 'action',
          render: (res, index) => (
            <div>
                <Button onClick={handleEdit} value={res.id} className="buttonedt" ><EditOutlined /></Button>
                <Button onClick={handleDelete} value={res.id} className="buttondlt" ><DeleteOutlined /></Button>
            </div>
          ),
        },
    ];
    const data = daftarMobile

    useEffect(() => {

        if (fetchStatus === false) {
            fetchData()
            setFetchStatus(true)
        }
    }, [fetchData, fetchStatus, setFetchStatus])

    const handleDelete = (event) => {
        let index = event.currentTarget.value
        functionDelete(index)
        console.log(index)
        // message.success('Data Terhapus')
    }
    const handleEdit= (event) => {
        let index = parseInt(event.currentTarget.value)
        history.push(`/Mobile-list/Form/${index}`);
        functionEdit(index)
        console.log(index)
    }
    const handleClick = () => {
      history.push(`/Mobile-list/Form`)
        setInput({
            name : '',
            description : '',
            category: '',
            release_year:2007,
            size:0,
            price: 0,
            rating : 0,
            image_url : '',
            is_android_app : 0,
            is_ios_app : 0
        })
    }

    return (
      <div className="row">
        <div className="section">
          <div className="card">
            <h1>Mobile Apps List</h1>
            <button onClick={handleClick}>Buat Daftar Nilai Mahasiswa Baru</button>
            <Table columns={columns} dataSource={data} />
          </div>
        </div>
      </div>  
    )
}

export default MobileList
