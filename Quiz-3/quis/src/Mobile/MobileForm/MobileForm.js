import React, { useContext, useEffect } from 'react'
import { MobilesContext } from '../../Context/MobileContext'
import { Form, Input, Button, Checkbox, message, InputNumber } from 'antd';
import { Link,useParams } from 'react-router-dom';

const MobileForm = () => {

    const { TextArea } = Input;

    const {input,setInput,
        currentIndex,setCurrentIndex,functions } = useContext(MobilesContext)

    const {functionSubmit,functionUpdate,fetchById} = functions
    let {Value} = useParams()

    useEffect(() => {
        if( Value !== undefined ){
            fetchById(Value)
        }
    },[])

    const handleCheckbox = (event) => {
        let checks = event.target.checked
        let nameOfInput = event.target.name

        setInput({[nameOfInput]:checks})
    }

    const handleChange = (event) => {
        let value = event.target.value
        let nameOfInput = event.target.name
        let ischeked = event.target.checked


        // setIsCheck(event.target.checked)
        console.log(ischeked)

        setInput({...input,[nameOfInput]:value,[nameOfInput]:ischeked})
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if(currentIndex === -1){
            functionSubmit(currentIndex)
        }else{
            functionUpdate(currentIndex)
        }
        setInput({
            name : '',
            description : '',
            category: '',
            release_year:2007,
            size:0,
            price: 0,
            rating : 0,
            image_url : '',
            is_android_app : 0,
            is_ios_app : 0
        })
        message.success('Data berhasil ditambah');
        setCurrentIndex(-1)

    }
    // const handleCheckbox = (event) => {
    //     console.log(event.target.value)
    //     let data = event.target.value
    //     data
    // }
    
    return (
        <div className="row">
            <div className="section">
                <div className="card">
                <Form
                    name="basic"
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 10 }}
                    initialValues={{ remember: true }}
                    method="post"
                    onFinish={handleSubmit}
                    autoComplete="off"
                    >
                    <Form.Item
                        label="Name"
                        name="name"
                        rules={[{ required: true, message: 'Please input name!' }]}
                    >
                        <Input
                            placeholder=" Masukan Nama"
                            value={input.name} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Category "
                        name="category"
                        rules={[{ required: true, message: 'Please input category!' }]}
                    >
                        <Input
                            placeholder=" Masukan Category"
                            value={input.category} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Description"
                        name="description"
                        rules={[{ required: true, message: 'Please input description!' }]}
                    >
                        <TextArea
                            placeholder="Masukan Description"
                            value={input.description} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Release Year"
                        name="release_year"
                        rules={[{ required: true, message: 'Please input year!' }]}
                    >
                        <InputNumber
                            min={2007}
                            max={2021}
                            placeholder="2007"
                            value={input.release_year} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Size(MB)"
                        name="size"
                        rules={[{ required: true, message: 'Please input size!' }]}
                    >
                        <InputNumber
                            placeholder=" Masukan size"
                            value={input.size} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Price"
                        name="price"
                        rules={[{ required: true, message: 'Please input price!' }]}
                    >
                        <InputNumber
                            placeholder=" Masukan price"
                            value={input.price} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Rating"
                        name="rating"
                        rules={[{ required: true, message: 'Please input rating!' }]}
                    >
                        <InputNumber
                            min={0}
                            max={5}
                            placeholder=" Masukan rating"
                            value={input.rating} 
                            onChange={handleChange}
                        />
                    </Form.Item>
                    <Form.Item
                        label="image url"
                        name="image_url"
                        rules={[{ required: true, message: 'Please input url!' }]}
                    >
                        <TextArea
                            placeholder=" Masukan url"
                            value={input.image_url} 
                            onChange={handleChange}
                        />
                    </Form.Item>

                    <Form.Item 
                        label="Platform"
                        valuePropName="checked" 
                        wrapperCol={{ span: 16 }}
                        rules={[{ required: true, message: 'Please check the box!' }]}
                    >
                        <Checkbox
                            name="is_android_app"
                            onChange={handleChange}
                            value = {input.is_android_app}
                            checked={input.is_android_app}
                        >Android</Checkbox><br/>
                        <Checkbox
                            name="is_ios_app"
                            onChange={handleChange}
                            value = {input.is_ios_app}
                            checked={input.is_ios_app}
                        >iOS</Checkbox>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                        Submit
                        </Button>
                    </Form.Item>
                    </Form>
                    <Link  to={'/Mobile-list'}>Kembali ke Tabel</Link>
                </div>
            </div>
        </div>  
            
    )
}

export default MobileForm
