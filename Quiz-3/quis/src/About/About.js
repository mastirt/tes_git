import React from 'react'

const Abouts = () => {
    return (
        <div className="row">
            <div className="section">
                <div className="card">
                    <h1>Data Peserta Jabarcodingcamp-2021 Reactjs</h1>
                    <p>1.<b>Nama : </b> Mahesa Tirta Panjalu</p>
                    <p>2.<b>Email : </b> Mahesat8@gmail.com</p>
                    <p>3.<b>Sistem Operasi yang digunakan : </b> Windows</p>
                    <p>4.<b>Akun Gitlab : </b> Mastirt</p>
                    <p>5.<b>Akun Telegram : </b> 087781308317</p>
                </div>
            </div>
        </div>
    )
}

export default Abouts
